# V4D: V4Design Project Ontology for reuse of 2D and 3D data
V4Design develops a platform that provides architects, video game creators and designers of any expertise with innovative tools necessary to enhance and simplify the creative phase of the designing process. A core idea behind V4Design is to reuse (i) 2D: movies, documentaries paintings and images from other artwork and (ii) 3D: printable models, 3D reconstructions and meshes and re-purpose it in a way that is useful for architecture and video game designers. This ontology is a part of WP5 to publish Linked Open Data.

### Ontology modules (TBox)
Please find the following TBox modules in the folder KULeuven/TBox

* V4D core: base module to be used during the V4D processing pipeline. This core is extended in four modules: v4dImage, v4dSfM, v4dSe and v4d3D .
	- base URI: https://w3id.org/v4d/core#
	- suggested prefix: `v4d`
* V4D Image: extension of V4D core, to be used during the image selection and preprocessing phase
	- base URI: https://w3id.org/v4d/Image#
	- suggested prefix: `v4dImage`
* V4D Structure from Motion (SfM): extension of V4D core, to be used during the SfM phase
	- base URI: https://w3id.org/v4d/SfM#
	- suggested prefix: `v4dSfM`
* V4D Semantic enrichment: extension of V4D core, to be used during the semantic enrichment (segmentation and classification) phase
	- base URI: https://w3id.org/v4d/SE#
	- suggested prefix: `v4dSe
* V4D 3D model selection: extension of V4D core, to be used during the 3D model selection and preprocessing phase
	- base URI: https://w3id.org/v4d/3D#
	- suggested prefix: `v4d3D`
* V4D building PRODUCT: extension of PRODUCT, to be used to classify building elements that are not yet defined in PRODUCT building element module (see https://github.com/w3c-lbd-cg/product)
	- base URI: https://w3id.org/v4d/Product#
	- suggested prefix: `v4dProduct`

### Data examples per V4D process phase (ABox)
Please find the following ABox modules in the folder KULeuven/ABox

* Example 1: Gendarmenmarkt (V4D Image-SfM-SemanticEnrichment phases)
* Example 2: WhiteHouse (V4D 3D-SemanticEnrichment phases)

The geometry that relates to these examples can be found on the following OSF page: https://osf.io/6y8vc/

### Project Website
https://v4design.eu/

### Authors
Maarten Bassier, KU Leuven [RG](https://www.researchgate.net/profile/Maarten_Bassier)

Mathias Bonduel, KU Leuven [RG](https://www.researchgate.net/profile/Mathias_Bonduel)

# funding
This project received funding from HORIZON 2020 - ICT - 779962
